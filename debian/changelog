sieve-extension (0.3.0+dfsg-1) unstable; urgency=medium

  * New upstream release, fixes compatibility with Thunderbird 60.4 and up.
  * Fix watchfile.
  * Update gbp.conf to reflect repo configuration.
  * Add new exclusions for repacking.
  * Refresh patches.
  * debian/copyright: Remove unused Expat license stanza.
  * Use debian/mainstscript instead of manual invocation in preinst.
  * Update Standards-Version with no changes.

 -- Martín Ferrari <tincho@debian.org>  Thu, 17 Jan 2019 17:20:12 +0000

sieve-extension (0.2.12+dfsg-1) unstable; urgency=medium

  * New upstream release. Closes: #876541, #870198, #808645.
  * debian/control:
    - Adopt the package. Closes: #899131.
    - Automated cme fixes; update Vcs locations.
    - Raise dh compat to 11.
  * debian/copyright: Update Files-Excluded.
  * Update patches.
  * Update watchfile.
  * Use dpkg-maintscript-helper. Closes: #858079.

 -- Martín Ferrari <tincho@debian.org>  Tue, 22 May 2018 21:00:08 +0000

sieve-extension (0.2.3h+dfsg-2) unstable; urgency=medium

  * Bump Standards-Version to 3.9.8.
  * Use https:// for copyright-format 1.0 URL.
  * Fix URL in Vcs-Git field.
  * Rebuild to also depend on thunderbird (Closes: #852637).
  * Replace codemirror copy with symlink to libjs-codemirror (Closes: #738748).
  * Add patch to remove donation button from UI (Closes: #782706).

 -- Michael Fladischer <fladi@debian.org>  Tue, 14 Feb 2017 15:51:41 +0100

sieve-extension (0.2.3h+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Bump debhelper version to 9.
  * Reformat packaging files with cme for better readability.

 -- Michael Fladischer <fladi@debian.org>  Mon, 21 Sep 2015 10:53:07 +0200

sieve-extension (0.2.3g+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Refresh unversion-jquery.patch.

 -- Michael Fladischer <fladi@debian.org>  Tue, 28 Jul 2015 18:47:32 +0200

sieve-extension (0.2.3f+dfsg-1) unstable; urgency=medium

  [ David Prévot ]
  * Adapt watch file for new amo scheme
  * Document upstream VCS

  [ Michael Fladischer ]
  * New upstream release.
  * Bump Standards-Version to 3.9.6.
  * Use github repository in d/watch to check for nightly builds.
  * Refresh unversion-jquery.patch.
  * Exclude minified jQuery from source tarball.
  * Exclude RFC documents from upstream source tarball.
  * Stop removing files no longer shipped in upstream tarball.
  * Remove unused sections from d/copyright.
  * Update Vcs-Browser to use cgit URL.
  * Change my email address to fladi@debian.org.

 -- Michael Fladischer <fladi@debian.org>  Wed, 08 Jul 2015 21:09:01 +0200

sieve-extension (0.2.3d-2~deb7u1) wheezy; urgency=medium

  * Team upload
  * Upload compatible version with recent icedove to stable
    (Closes: #746039)

 -- David Prévot <taffit@debian.org>  Thu, 29 May 2014 18:09:59 -0400

sieve-extension (0.2.3d-2) unstable; urgency=medium

  * Add preinst script to remove symlinked codemirror (Closes: #739145).
  * Add codemirror sieve mode license to d/copyright.
  * Do not install codemirror and sieve mode licenses.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Fri, 21 Feb 2014 12:16:33 +0100

sieve-extension (0.2.3d-1) unstable; urgency=medium

  * Imported Upstream version 0.2.3d (Closes: #728522).
  * Use embedded CodeMirror instead of the outdated and incomaptible packaged
    one.
  * Bump Standards-Version to 3.9.5.
  * Add unversion-jquery.patch to include an unversioned jQuery file.
  * Update years in d/copyright.
  * Change jQuery path in d/copyright and d/links.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Wed, 12 Feb 2014 08:46:15 +0100

sieve-extension (0.2.2+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Drop included minified jQuery file without source to comply to DFSG.
  * Bump to standards version 3.9.4.
  * Use canonical URI in Vcs-* fields.
  * Depend on libjs-jquery.
  * Depend on libjs-codemirror.
  * Replace included codemirror with symlinks to libjs-codemirror.
  * Replace included jQuery with symlink to libjs-jquery.
  * Remove superfluous copy of SieveOverlayManager.jsm.
  * Remove empty directories.
  * Use ftp.mozilla.org for d/watch to find newer versions.
  * Add .gitignore to ignore quilt directory.
  * Update year in d/copyright.
  * Make Build-Depends on mozilla-devscripts unversioned.
  * Change Source field in d/copyright to
    https://addons.mozilla.org/en-US/thunderbird/addon/sieve/.
  * Change Upstream-Name field in d/copyright to sieve.
  * Add Breaks: ${xpi:Breaks}.
  * Fix typos in description.
  * Mention license for CodeMirror.
  * Mention license for jQuery.
  * Include full text for AGPL-3 in d/copyright.
  * Include and install upstream changelog, taken from upstream git.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Wed, 17 Jul 2013 14:26:15 +0200

sieve-extension (0.1.14-1) unstable; urgency=low

  * New upstream release.
  * Bump to standards version 3.9.3.
  * Update DEP-5 URL to 1.0.
  * Update years in d/copyright.
  * Change order of my name.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Tue, 28 Feb 2012 09:53:19 +0100

sieve-extension (0.1.12-1) unstable; urgency=low

  * New upstream release.
  * Bumped Standards-Version to 3.9.2 (no change necessary).

 -- Fladischer Michael <FladischerMichael@fladi.at>  Sun, 05 Jun 2011 08:23:02 +0200

sieve-extension (0.1.11-1) unstable; urgency=low

  * New upstream release.
  * Move to section mail.
  * Use correct package name in copyright file.
  * Update DEP-5 format.

 -- Fladischer Michael <FladischerMichael@fladi.at>  Tue, 08 Mar 2011 22:11:52 +0100

sieve-extension (0.1.10-2) unstable; urgency=low

  * Change source package name to comply with pkg-mozext.

 -- Fladischer Michael <FladischerMichael@fladi.at>  Wed, 04 Aug 2010 11:38:54 +0200

icedove-sieve (0.1.10-1) unstable; urgency=low

  * Initial release. (Closes: #589342)

 -- Fladischer Michael <FladischerMichael@fladi.at>  Thu, 29 Jul 2010 08:43:20 +0200
